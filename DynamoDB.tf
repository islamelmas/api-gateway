resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = "PersonsTable"
  billing_mode   = "PROVISIONED"
  read_capacity  = 1000
  write_capacity = 20
  hash_key       = "ID"
  attribute {
    name = "ID"
    type = "N"
  }

}
resource "aws_dynamodb_table_item" "example" {
  table_name = aws_dynamodb_table.basic-dynamodb-table.name
  hash_key   = aws_dynamodb_table.basic-dynamodb-table.hash_key

  item = <<ITEM
{
  "ID": {"N": "0"},
  "Name": {"S": "Islam"}
}
ITEM
}