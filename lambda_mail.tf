resource "aws_lambda_function" "lambda_mail" {
  function_name = "myEmail"
  description   = "Send an email everytime lambda is invoked"
  memory_size = 1024
  filename         = "lambda_mail.zip"
  runtime          = "python3.8"
  role             = "${aws_iam_role.lambda_role_mail.arn}"
  handler          = "mail.lambda_handler"


}

resource "aws_iam_role" "lambda_role_mail" {
  name = "Lambda-Role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}