resource "aws_iam_role" "lambda_role" {
  name = "Lambda_Role_Full_Control"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "policy" {
  name        = "DynamoDBFullAccess"
  description = "policy1"

  policy ="${file("iam/DynamoPolicy.json")}"
}
resource "aws_iam_policy" "policy2" {
  name        = "AllowExecutionPolicy"
  description = "policy2"

  policy ="${file("iam/ExecutionPolicy.json")}"
}
resource "aws_iam_policy" "policy3" {
  name        = "AllowInvokerLambda"
  description = "policy3"

  policy ="${file("iam/Invoker.json")}"
}
  
resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.policy.arn
}
resource "aws_iam_role_policy_attachment" "test2-attach" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.policy2.arn
}

resource "aws_iam_role_policy_attachment" "test3-attach" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.policy3.arn
}
