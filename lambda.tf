resource "aws_lambda_function" "GetCreatePerson" {
  function_name = "GetCreatePersons"
  description   = "connect to dynamo Db and http api gateway to create and get data"
  memory_size = 1024
  filename         = "terraform_function.zip"
  runtime          = "python3.8"
  role             = "${aws_iam_role.lambda_role.arn}"
  handler          = "lambda_function.lambda_handler"


}