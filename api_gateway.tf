provider "aws" {
  region                  = "us-west-2"
  shared_credentials_file = "/home/islam/.aws/credentials"
  profile                 = "default"
}

resource "aws_apigatewayv2_api" "persons" {
  name          = "persons"
  protocol_type = "HTTP"
}
resource "aws_apigatewayv2_deployment" "example" {
  api_id      = aws_apigatewayv2_route.example.api_id
  description = "Example deployment"

  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_apigatewayv2_stage" "stage_test" {
  api_id = aws_apigatewayv2_api.persons.id
  name   = "test"
  auto_deploy= true
  default_route_settings{
    throttling_burst_limit="1000"
    throttling_rate_limit="1000"
    }
}
  
output "URL_API" {
  value = aws_apigatewayv2_stage.stage_test.invoke_url
}
resource "aws_apigatewayv2_route" "example" {
  api_id    = aws_apigatewayv2_api.persons.id
  route_key = "POST /createPerson"

  target = "integrations/${aws_apigatewayv2_integration.post.id}"
 }
resource "aws_apigatewayv2_route" "example2" {
  api_id    = aws_apigatewayv2_api.persons.id
  route_key = "GET /getPerson"

  target = "integrations/${aws_apigatewayv2_integration.post.id}"
}
resource "aws_apigatewayv2_integration" "post" {
  api_id           = aws_apigatewayv2_api.persons.id
  credentials_arn     = aws_iam_role.apigateway_role.arn
  integration_type = "AWS_PROXY"
  connection_type           = "INTERNET"
  payload_format_version    = "2.0"
  description               = "Lambda Post to DB"
  #integration_method        = "POST"
  integration_uri           = aws_lambda_function.GetCreatePerson.invoke_arn
  passthrough_behavior      = "WHEN_NO_MATCH"
}
