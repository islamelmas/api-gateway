import boto3

client = boto3.client('dynamodb')
client2= boto3.client('lambda')

def lambda_handler(event, context):
    if event["rawPath"]=="/test/getPerson":
        print("I am in getPerson")
        id=event["queryStringParameters"]["ID"]
        #print(id)
        #print(event)
        response = client.get_item(TableName='PersonsTable',Key={'ID': {'N':id}})
        response2=client2.invoke(FunctionName='arn:aws:lambda:us-west-2:892272961430:function:myEmail',InvocationType='Event')
        print(response)
        name=response["Item"]["Name"]["S"]
        return "Name is "+name
    elif event["rawPath"]=="/test/createPerson":
        print("I am in CreatePerson")
        id=event["queryStringParameters"]["ID"]
        name=event["queryStringParameters"]["Name"]
        response=client.put_item(TableName='PersonsTable',Item={'ID': {'N':id},'Name':{'S':name}})
        response=client2.invoke(FunctionName='arn:aws:lambda:us-west-2:892272961430:function:myEmail',InvocationType='Event')
        return "Insertion Done"
     

   
   
 
    