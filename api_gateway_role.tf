resource "aws_iam_role" "apigateway_role" {
    name = "APiGatewayInvokeLambda-Role"
  
    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "test4-attach" {
  role       = aws_iam_role.apigateway_role.name
  policy_arn = aws_iam_policy.policy3.arn
}

